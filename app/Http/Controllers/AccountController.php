<?php

namespace Docler\Http\Controllers;

use Illuminate\Auth\Guard;

class AccountController extends Controller
{
    protected $guard;

    /**
     * Create a new controller instance.
     *
     * @param Guard $guard
     */
    public function __construct(Guard $guard)
    {
        $this->guard = $guard;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        return view('home', [
            'user' => $this->guard->user()
        ]);
    }

}
