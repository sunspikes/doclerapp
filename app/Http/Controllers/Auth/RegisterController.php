<?php

namespace Docler\Http\Controllers\Auth;

use Docler\Http\Controllers\Controller;
use Docler\Services\Contracts\RegistrationContract;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RegisterController extends Controller
{
    /**
     * The login path.
     *
     * @var string
     */
    protected $loginPath = '/login';

    /**
     * @var RegistrationContract
     */
    protected $register;

    /**
     * Create a new authentication controller instance.
     *
     * @param RegistrationContract $register
     */
    public function __construct(RegistrationContract $register)
    {
        $this->register = $register;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param Request|\Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->register->validate();

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->register->create();
        $this->register->sendVerificationMail($user);

        $request->session()->flash('status', 'Please check your email to complete verification!');

        return redirect($this->loginPath);
    }

    /**
     * Verify the user email.
     *
     * @param Request $request
     * @param $uid
     * @param  string $token
     * @return \Illuminate\Http\Response
     * @throws NotFoundHttpException
     */
    public function getVerify(Request $request, $uid, $token = null)
    {
        if (is_null($token) || ! $this->register->verifyActivation($uid, $token)) {
            throw new NotFoundHttpException;
        }

        $request->session()->flash('status', 'Successfully verified your email!');

        return redirect($this->loginPath);
    }
}