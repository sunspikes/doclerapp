<?php

namespace Docler\Http\Controllers\Auth;

use Docler\Http\Controllers\Controller;
use Docler\Services\Contracts\AuthenticationContract;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * The guard instance.
     *
     * @var Guard
     */
    private $guard;

    /**
     * The account redirect path.
     *
     * @var string
     */
    protected $redirectPath = '/home';

    /**
     * The login path.
     *
     * @var string
     */
    protected $loginPath = '/login';

    /**
     * The authentication service.
     *
     * @var AuthenticationContract
     */
    protected $auth;

    /**
     * Create a new authentication controller instance.
     *
     * @param Guard $guard
     * @param AuthenticationContract $auth
     */
    public function __construct(Guard $guard, AuthenticationContract $auth)
    {
        $this->guard = $guard;
        $this->auth = $auth;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param Request|\Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $validator = $this->auth->validateLogin();

        if (! $validator->fails()) {
            if ($this->auth->validateVerification($request->only('email'))) {
                if ($this->guard->attempt($request->only('email', 'password'), $request->has('remember'))) {
                    return $this->auth->handleLoginSuccess($this->redirectPath);
                }

                $validator = ['email' => 'Invalid login credentials'];
            }
            else {
                $validator = ['email' => 'Please verify your email to log in.'];
            }
        }

        $this->auth->handleLoginFailed();

        return redirect($this->loginPath)
            ->withInput($request->only('email', 'remember'))
            ->withErrors($validator);
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        $this->guard->logout();

        return redirect($this->loginPath);
    }
}
