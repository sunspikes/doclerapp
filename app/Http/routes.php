<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Auth\AuthController@getLogin');

Route::get('register', 'Auth\RegisterController@getRegister');
Route::post('register', 'Auth\RegisterController@postRegister');
Route::get('verify/{uid}/{token}', 'Auth\RegisterController@getVerify');

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');

Route::get('logout', 'Auth\AuthController@getLogout');

Route::get('forgot-password', 'Auth\PasswordController@getEmail');
Route::post('forgot-password', 'Auth\PasswordController@postEmail');

Route::get('password-reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password-reset', 'Auth\PasswordController@postReset');

Route::get('home', 'AccountController@index');
