<?php

namespace Docler\Providers;

use Docler\User;
use Docler\Services\Registration;
use Illuminate\Support\ServiceProvider;
use Docler\Services\Authentication;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Docler\Services\Contracts\AuthenticationContract', function($app)
        {
            return new Authentication(
                $app['config'],
                $app['cache.store'],
                $app['throttle'],
                $app['request'],
                $app['user'],
                $app['validator']
            );
        });

        $this->app->bind('Docler\Services\Contracts\RegistrationContract', function($app)
        {
            return new Registration(
                $app['request'],
                $app['validator'],
                $app['user'],
                $app['mailer']
            );
        });

        $this->app->bind('user', function($app)
        {
            return new User();
        });
    }
}
