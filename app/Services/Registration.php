<?php

namespace Docler\Services;

use Docler\Services\Contracts\RegistrationContract;
use Docler\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Str;

/**
 * Docler\Services\Registration
 *
 * The Registration helper service
 *
 * Validates, verifies and performs user registration.
 */
class Registration implements RegistrationContract
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Validator
     */
    protected $validator;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Mailer
     */
    protected $mailer;

    public function __construct(Request $request,
                                Validator $validator,
                                User $user,
                                Mailer $mailer)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->user = $user;
        $this->mailer = $mailer;
    }

    /**
     * @inheritdoc
     */
    public function validate()
    {
        return $this->validator->make($this->request->all(), $this->user->rules);
    }

    /**
     * @inheritdoc
     */
    public function create()
    {
        $data = $this->request->all();

        return $this->user->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'verification' => hash_hmac('sha256', Str::random(40), Str::random(40)),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function sendVerificationMail(User $user)
    {
        $this->mailer->send('emails.verification', ['user' => $user], function($mail) use ($user) {
            $mail->to($user->email, $user->name)
                ->subject('Docler App - Verify your account.');
        });
    }

    /**
     * @inheritdoc
     */
    public function verifyActivation($uid, $token)
    {
        if ( ($user = $this->user->find(intval($uid))) && ($user->verification == $token) ) {
            // Reset verification token
            $user->verification = '';
            $user->save();

            return true;
        }

        return false;
    }


}