<?php

namespace Docler\Services;

use Docler\User;
use Docler\Services\Contracts\AuthenticationContract;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Http\Request;
use Illuminate\Config\Repository as Config;
use sunspikes\Throttle\Throttle;
use Illuminate\Contracts\Cache\Repository as Cache;

/**
 * Docler\Services\Authentication
 *
 * The authentication helper service.
 *
 * Validates, verifies, throttles and
 * handles authentication success and failures.
 */
class Authentication implements AuthenticationContract
{
    /**
     * @var Config
     */
    protected $config;
    /**
     * @var Cache
     */
    protected $cache;
    /**
     * @var Throttle
     */
    protected $throttle;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var Validator
     */
    protected $validator;
    /**
     * @var User
     */
    protected $user;
    /**
     * @var array
     */
    protected $throttlers;
    /**
     * @var const CACHE_EXPIRY
     */
    const CACHE_EXPIRY = 525949;

    public function __construct(Config $config,
                                Cache $cache,
                                Throttle $throttle,
                                Request $request,
                                User $user,
                                Validator $validator)
    {
        $this->config = $config;
        $this->cache = $cache;
        $this->throttle = $throttle;
        $this->request = $request;
        $this->user = $user;
        $this->validator = $validator;
    }

    /**
     * Initialize all the throttlers
     */
    private function initThrottlers()
    {
        $email = $this->request->input('email', '');
        $ip = $this->request->getClientIp();

        if ($email) {
            $this->throttlers['user'] = $this->throttle->get([
                'ip' => '',
                'email' => $email
                ],
                $this->config->get('app.throttle_user_attempts'),
                $this->config->get('app.throttle_validity')
            );
        }

        if ($ip) {
            $classBMask = $this->applyClassBMask($ip);
            $classCMask = $this->applyClassCMask($ip);

            $this->throttlers['ip'] = $this->throttle->get([
                'ip' => $ip,
                'email' => ''
                ],
                $this->config->get('app.throttle_ip_attempts'),
                $this->config->get('app.throttle_validity')
            );

            $this->throttlers['class_b'] = $this->throttle->get([
                'ip' => $classBMask,
                'email' => ''
                ],
                $this->config->get('app.throttle_class_b_attempts'),
                $this->config->get('app.throttle_validity')
            );

            $this->throttlers['class_c'] = $this->throttle->get([
                'ip' => $classCMask,
                'email' => ''
                ],
                $this->config->get('app.throttle_class_c_attempts'),
                $this->config->get('app.throttle_validity')
            );
        }
    }

    /**
     * Applies class B mask (255.255.0.0)
     *
     * @param string $ip
     * @return string
     */
    private function applyClassBMask($ip)
    {
        return preg_replace('/\.\d+\.\d+$/', '.0.0', $ip);
    }

    /**
     * Applies class C mask (255.255.255.0)
     *
     * @param string $ip
     * @return string
     */
    private function applyClassCMask($ip)
    {
        return preg_replace('/\.\d+$/', '.0', $ip);
    }

    /**
     * @inheritdoc
     */
    public function validateVerification($email)
    {
        $user = $this->user->where('email', $email)->first();

        if ($user['verification']) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function validateLogin()
    {
        $this->initThrottlers();

        $rules = [
            'email' => 'email|required',
            'password' => 'required',
        ];

        if ($this->cache->get('showCaptcha', false)) {
            $rules['g-recaptcha-response'] = 'required|recaptcha';
        }

        return $this->validator->make($this->request->all(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function handleLoginFailed()
    {
        if ($this->throttlers) {
            if (!$this->cache->get('showCaptcha', false)) {
                foreach ($this->throttlers as $throttler) {
                    $throttler->hit();
                }
            } else {
                if ($this->throttlers['user']) {
                    $this->throttlers['user']->hit();
                }

                if ($this->throttlers['ip']) {
                    $this->throttlers['ip']->hit();
                }
            }
        }

        $this->toggleCaptcha();
    }

    /**
     * @inheritdoc
     */
    public function handleLoginSuccess($redirectPath)
    {
        return redirect()->intended($redirectPath);
    }

    /**
     * Toggle the display of captcha
     */
    private function toggleCaptcha()
    {
        $show = false;

        if ($this->throttlers) {
            foreach ($this->throttlers as $throttler) {
                if (!$throttler->check()) {
                    $show = true;
                }
            }
        }

        $this->cache->put('showCaptcha', ($this->cache->get('showCaptcha', false) ?: $show), self::CACHE_EXPIRY);
    }
}