<?php

namespace Docler\Services\Contracts;

use Docler\User;
use Illuminate\Http\Request;

interface RegistrationContract
{
    /**
     * Validate user registration
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validate();

    /**
     * Register (create) user
     *
     * @return User $user
     */
    public function create();

    /**
     * Send user verification email
     *
     * @param User $user
     */
    public function sendVerificationMail(User $user);

    /**
     * Verify user activation
     *
     * @param int $uid
     * @param string $token
     * @return bool
     */
    public function verifyActivation($uid, $token);
}