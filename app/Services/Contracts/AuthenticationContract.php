<?php

namespace Docler\Services\Contracts;

use Illuminate\Http\Request;

interface AuthenticationContract
{
    /**
     * Check if user user has verfied the email
     *
     * @param string
     * @return bool
     */
    public function validateVerification($email);

    /**
     * Validate user credentials on login
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validateLogin();

    /**
     * Handle the login failure
     */
    public function handleLoginFailed();

    /**
     * Handle the login success
     *
     * @param string $redirectPath
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleLoginSuccess($redirectPath);
}