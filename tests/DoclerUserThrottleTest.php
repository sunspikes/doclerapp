<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DoclerUserThrottleTest extends TestCase
{
    /**
     * Test user throttle.
     *
     * @return void
     */
    public function testUserEmailThrottle()
    {
        $this->visit('/')
            ->type('sunspikes@gmail.com', 'email')
            ->type('wrongpass', 'password')
            ->press('Login')
            ->see('Login');
        $this->visit('/')
            ->type('sunspikes@gmail.com', 'email')
            ->type('wrongpass', 'password')
            ->press('Login')
            ->see('Login');
        $this->visit('/')
            ->type('sunspikes@gmail.com', 'email')
            ->type('wrongpass', 'password')
            ->press('Login')
            ->see('g-recaptcha');

    }

    /**
     * Test ip throttle.
     *
     * @return void
     */
    public function testUserIpThrottle()
    {
        $this->visit('/')
            ->type('', 'email')
            ->type('', 'password')
            ->press('Login')
            ->see('Login');
        $this->visit('/')
            ->type('', 'email')
            ->type('', 'password')
            ->press('Login')
            ->see('Login');
        $this->visit('/')
            ->type('', 'email')
            ->type('', 'password')
            ->press('Login')
            ->see('g-recaptcha');

    }
}