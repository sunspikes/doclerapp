<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery as m;
use Docler\Services\Authentication;
use sunspikes\Throttle\Throttle;

class DoclerRangeThrottleTest extends TestCase
{
    /**
     * setUp test environment.
     */
    public function setUp()
    {
        parent::setUp();
        
        $this->config = m::mock('Illuminate\Config\Repository');
        $this->config->shouldReceive('get')->with('app.throttle_validity')->andReturn(60);
        $this->config->shouldReceive('get')->with('app.throttle_user_attempts')->andReturn(3);
        $this->config->shouldReceive('get')->with('app.throttle_ip_attempts')->andReturn(3);
        $this->config->shouldReceive('get')->with('app.throttle_class_b_attempts')->andReturn(1000);
        $this->config->shouldReceive('get')->with('app.throttle_class_c_attempts')->andReturn(500);
        
        $reflection = new \ReflectionClass('Docler\Services\Authentication');
        $this->initThrottlers = $reflection->getMethod('initThrottlers');
        $this->initThrottlers->setAccessible(true);
        $this->throttlersProperty = $reflection->getProperty('throttlers');
        $this->throttlersProperty->setAccessible(true);
    }
    
    /**
     * Test successful class c throttling.
     *
     * @return void
     */
    public function testClassCThrottleSuccess()
    {
        for ($i = 1; $i <= 500; $i++) {
            $request = m::mock('Illuminate\Http\Request');
            $request->shouldReceive('input')->andReturn('');
            $request->shouldReceive('getClientIp')->andReturn('192.168.49.'. $i);
            
            $auth = new Authentication(
                    $this->config,
                    m::mock('Illuminate\Contracts\Cache\Repository'),
                    App::make('throttle'),
                    $request,
                    m::mock('Docler\User'),
                    m::mock('Illuminate\Validation\Factory')
                );
           
            $this->initThrottlers->invoke($auth);
            $this->throttlers = $this->throttlersProperty->getValue($auth);

            foreach ($this->throttlers as $throttler) {
                $throttler->hit();                
            }
        }
        
        $this->assertFalse($this->throttlers['class_c']->check()); 
    }

    /**
     * Test failed class c throttling.
     *
     * @return void
     */
    public function testClassCThrottleFailure()
    {
        for ($i = 1; $i < 500; $i++) {
            $request = m::mock('Illuminate\Http\Request');
            $request->shouldReceive('input')->andReturn('');
            $request->shouldReceive('getClientIp')->andReturn('192.168.48.'. $i);
            
            $auth = new Authentication(
                    $this->config,
                    m::mock('Illuminate\Contracts\Cache\Repository'),
                    App::make('throttle'),
                    $request,
                    m::mock('Docler\User'),
                    m::mock('Illuminate\Validation\Factory')
                );
           
            $this->initThrottlers->invoke($auth);
            $this->throttlers = $this->throttlersProperty->getValue($auth);

            foreach ($this->throttlers as $throttler) {
                $throttler->hit();                
            }
        }
        
        $this->assertTrue($this->throttlers['class_c']->check()); 
    }

    /**
     * Test successful class b throttling.
     *
     * @return void
     */
    public function testClassBThrottleSuccess()
    {
        for ($i = 1; $i <= 1000; $i++) {
            $request = m::mock('Illuminate\Http\Request');
            $request->shouldReceive('input')->andReturn('');
            $request->shouldReceive('getClientIp')->andReturn('192.168.'. rand(1, 999) .'.'. rand(1, 999));
            
            $auth = new Authentication(
                    $this->config,
                    m::mock('Illuminate\Contracts\Cache\Repository'),
                    App::make('throttle'),
                    $request,
                    m::mock('Docler\User'),
                    m::mock('Illuminate\Validation\Factory')
                );
           
            $this->initThrottlers->invoke($auth);
            $this->throttlers = $this->throttlersProperty->getValue($auth);

            foreach ($this->throttlers as $throttler) {
                $throttler->hit();                
            }
        }
        
        $this->assertFalse($this->throttlers['class_b']->check()); 
    }

    /**
     * Test failed class b throttling.
     *
     * @return void
     */
    public function testClassBThrottleFailure()
    {
        for ($i = 1; $i < 1000; $i++) {
            $request = m::mock('Illuminate\Http\Request');
            $request->shouldReceive('input')->andReturn('');
            $request->shouldReceive('getClientIp')->andReturn('192.168.'. rand(1, 999) .'.'. rand(1, 999));
            
            $auth = new Authentication(
                    $this->config,
                    m::mock('Illuminate\Contracts\Cache\Repository'),
                    App::make('throttle'),
                    $request,
                    m::mock('Docler\User'),
                    m::mock('Illuminate\Validation\Factory')
                );
           
            $this->initThrottlers->invoke($auth);
            $this->throttlers = $this->throttlersProperty->getValue($auth);

            foreach ($this->throttlers as $throttler) {
                $throttler->hit();                
            }
        }
        
        $this->assertTrue($this->throttlers['class_b']->check()); 
    }
    
    /**
     * Clear the throttle cache
     */
    public function tearDown()
    {
        parent::tearDown();
        
        foreach ($this->throttlers as $throttler) {
            $throttler->clear();                
        }
    }
}