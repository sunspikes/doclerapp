<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DoclerPagesTest extends TestCase
{
    /**
     * Test login page.
     *
     * @return void
     */
    public function testLoginPage()
    {
        $this->visit('/')
            ->see('Login');
    }

    /**
     * Test successful login and logout
     *
     * @return void
     */
    public function testLoginLogoutSuccess()
    {
        $this->visit('/')
            ->type('sunspikes@gmail.com', 'email')
            ->type('Test1234@', 'password')
            ->press('Login')
            ->see('Welcome Prasad!')
            ->click('Logout')
            ->see('Login');
    }

    /**
     * Test registration page.
     *
     * @return void
     */
    public function testRegistrationPage()
    {
        $this->visit('/')
            ->click('Create new account')
            ->see('Register');
    }

    /**
     * Test registration page.
     *
     * @return void
     */
    public function testForgotPasswordPage()
    {
        $this->visit('/')
            ->click('Forgot Your Password?')
            ->see('Reset Password');
    }

    /**
     * Test new account registration and login
     *
     * @return void
     */
    public function testRegisterLogin()
    {
        $faker = \Faker\Factory::create();
        $email = $faker->email;

        $this->visit('/register')
            ->type($faker->name, 'name')
            ->type($email, 'email')
            ->type('Test1234@', 'password')
            ->type('Test1234@', 'password_confirmation')
            ->press('Register')
            ->see('Please check your email to complete verification!');

        $this->visit('/')
            ->type($email, 'email')
            ->type('Test1234@', 'password')
            ->press('Login')
            ->see('Please verify your email to log in.');
    }
}
