@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Unauthorized.</div>
                    <div class="panel-body">
                        <p>You are not authorized to access this page.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection