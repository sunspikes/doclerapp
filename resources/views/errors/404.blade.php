@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Page not found.</div>
                    <div class="panel-body">
                        <p>The requested URL was not found on this server.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection